package com.hibernate4all.tutorial.converter;

import com.hibernate4all.tutorial.domain.Certification;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;

@Converter(autoApply = true)
public class CertificationAttributeConverter implements AttributeConverter<Certification, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Certification certification) {
        return certification != null ? certification.getKey() : null;
    }

    @Override
    public Certification convertToEntityAttribute(Integer dbKey) {
        return Arrays.stream(Certification.values())
                .filter((Certification certification) -> certification.getKey().equals(dbKey))
                .findFirst().orElse(null);
    }

}
