package com.hibernate4all.tutorial.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String author;
    private String content;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Integer getId() {
        return id;
    }

    public Review setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Review setAuthor(String author) {
        this.author = author;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Review setContent(String content) {
        this.content = content;
        return this;
    }

    public Movie getMovie() {
        return movie;
    }

    public Review setMovie(Movie movie) {
        this.movie = movie;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;
        Review review = (Review) o;
        return getId().equals(review.getId())
                && getAuthor().equals(review.getAuthor())
                && getContent().equals(review.getContent())
                && getMovie().equals(review.getMovie());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAuthor(), getContent(), getMovie());
    }
}
