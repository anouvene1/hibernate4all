package com.hibernate4all.tutorial.domain;

public enum Certification {
    TOUS_PUBLIC(0, "Tous public"),
    INTERDIT_MOINS_12(1, "Interdit au moins de 12 ans"),
    INTERDIT_MOINS_16(2, "Interdit au moins de 16 ans"),
    INTERDIT_MOINS_18(3, "Interdit au moins de 18 ans");

    Certification(Integer key, String description) {
        this.key = key;
        this.description = description;
    }

    private Integer key;
    private String description;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
