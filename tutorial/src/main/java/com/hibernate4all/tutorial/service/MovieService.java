package com.hibernate4all.tutorial.service;

import com.hibernate4all.tutorial.domain.Movie;
import com.hibernate4all.tutorial.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovieService {
    private MovieRepository mr;
    @Autowired
    public MovieService(MovieRepository mr) {
        this.mr = mr;
    }

    @Transactional
    public void updateMovieDescription(Long id, String description) {
        Movie mv = mr.find(id);
        mv.setDescription(description);
    }

    @Transactional
    public List<Movie> addMovieThenGetAll() {
        Movie movie = new Movie();
        movie.setName("Rambo");
        movie.setDescription("Avec Sylvest Stalonne");
        this.mr.persist(movie);
        return this.mr.getAll();
    }
}
