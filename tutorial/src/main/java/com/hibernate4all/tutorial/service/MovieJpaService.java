package com.hibernate4all.tutorial.service;

import com.hibernate4all.tutorial.domain.Certification;
import com.hibernate4all.tutorial.domain.Movie;
import com.hibernate4all.tutorial.repository.MovieJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;



@Service
public class MovieJpaService {
    private final MovieJpaRepository movieJpaRepository;
    private final static Logger LOGGER = LoggerFactory.getLogger(MovieJpaService.class);

    public MovieJpaService(MovieJpaRepository movieJpaRepository) {
        this.movieJpaRepository = movieJpaRepository;
    }

    @Transactional
    public Movie saveMovie(Movie movie) {
        return this.movieJpaRepository.save(movie); // persist
    }

    public Optional<Movie> getMovie(Long movieId) {
        return this.movieJpaRepository.findById(movieId); // Proxy qui récupère une référence et donc risque de pb avec Jackson
        // find, findById ...
    }

    public Movie findMovie(Long movieId) {
        return this.movieJpaRepository.getOne(movieId);
    }

    public List<Movie> getAllMovies() {
        return this.movieJpaRepository.findAll();
    }

    @Transactional
    public Movie updateMovie(Movie movie) {
        Optional<Movie> movieToUpdate = this.getMovie(movie.getId());
        Movie updatedMovie = null;
        if(movieToUpdate != null) {
            updatedMovie = this.movieJpaRepository.saveAndFlush(movie); // merge (si existe pas alors insert)
        }
        return updatedMovie;
    }
    @Transactional
    public Integer updateMovieDescription(String description, Integer certifKey, Long movieId) {
        return this.movieJpaRepository.updateMovieDescription(description, Arrays.stream(Certification.values()).filter(certification -> certification.getKey().equals(certifKey)).findFirst().orElse(null), movieId);
    }

    @Transactional
    public void deleteMovie(Long movieId) {
        Movie movieToDelete = this.movieJpaRepository.getOne(movieId);
        if(movieToDelete != null) {
            LOGGER.trace("Movie success delete: ", movieToDelete);
            this.movieJpaRepository.delete(movieToDelete);
        } else {
            LOGGER.trace("Movie error delete");
        }
    }
}
