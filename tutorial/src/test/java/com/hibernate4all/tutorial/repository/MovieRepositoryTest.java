package com.hibernate4all.tutorial.repository;

import com.hibernate4all.tutorial.config.PersistenceConfig;
import com.hibernate4all.tutorial.domain.Certification;
import com.hibernate4all.tutorial.domain.Movie;
import com.hibernate4all.tutorial.domain.Review;
import com.hibernate4all.tutorial.service.MovieService;
import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

//@RunWith(SpringRunner.class) // Junit version < 5
// @SpringBootTest(classes = {
//         TutorialApplication.class,
//         PersistenceConfig.class})
//@ActiveProfiles("test")
@ExtendWith(SpringExtension.class) // Contexte de Spring
@ContextConfiguration(classes = {PersistenceConfig.class}) // Classes de config dont a besoin Spring Contexte pour s'initialiser
@SqlConfig(dataSource = "dataSourceH2", transactionManager = "transactionManager")
@Sql("/datas/datas.sql")
public class MovieRepositoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieRepositoryTest.class);

    @Autowired
    private MovieRepository repository;

    @Autowired
    private MovieService movieService;

    @Test
    public void saveTest() {
        Movie movie = new Movie().setName("Inception 2")
                .setDescription("Dom Cobb est un voleur expérimenté dans l'art périlleux de `l'extraction' : sa spécialité consiste à s'approprier les secrets les plus précieux d'un individu ...")
                .setCertification(Certification.INTERDIT_MOINS_12);
        repository.persist(movie);
    }

    @Test
    public void associationMovieReviewCreateTest() {
        Review review1 = new Review().setAuthor("Tuan").setContent("Comédie déjantée à voir entre amis ...");
        Review review2 = new Review().setAuthor("Antoine").setContent("Un bon moment de cinéma ...");

        Movie movie = new Movie().setName("Everything Everywhere All at Once")
                .setDescription("Evelyn Wang est à bout : elle ne comprend plus sa famille, son travail et croule sous les impôts …")
                .setCertification(Certification.TOUS_PUBLIC)
                .addReview(review1)
                .addReview(review2);

        repository.persist(movie);
    }

    @Test
    public void associationMovieReviewGetTest() {
        assertThrows(LazyInitializationException.class, () -> {
            Movie movie = repository.find(-1L);
            LOGGER.trace("Nombre de reviews: " +  movie.getReviews().size());
        });
    }

    @Test
    public void findTest() {
        Movie movie = repository.find(-3L);
        //assertThat(movie.getName()).as("Mauvais film récupéré !!!").isEqualTo("Memento");
        assertThat(movie.getCertification()).as("Le converter fonctionne").isEqualTo(Certification.INTERDIT_MOINS_16);
    }

    @Test
    public void getAllTest() {
        List<Movie> movies = repository.getAll();
        //assertThat(movies.size()).as("Tous les films ne sont pas récupérés !!!").isEqualTo(4);
        assertThat(movies).as("Tous les films ne sont pas récupérés !!!").hasSize(4);
    }

    @Test
    public void flushModeTest() {
        List<Movie> movies = movieService.addMovieThenGetAll();
        assertThat(movies).as("On devrait avoir 4 movies existant dans la bdd + une nouvelle").hasSize(5);
    }
}
