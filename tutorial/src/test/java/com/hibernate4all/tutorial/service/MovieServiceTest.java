package com.hibernate4all.tutorial.service;

import com.hibernate4all.tutorial.config.PersistenceConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class) // Contexte de Spring
@ContextConfiguration(classes = {PersistenceConfig.class}) // Classes de config dont a besoin Spring Contexte pour s'initialiser
@SqlConfig(dataSource = "dataSourceH2", transactionManager = "transactionManager")
@Sql("/datas/datas.sql")
public class MovieServiceTest {
    private MovieService movieService;

    @Autowired
    public MovieServiceTest(MovieService ms) {
        this.movieService = ms;
    }

    @Test
    public void updateDescription_casNominal() {
        movieService.updateMovieDescription(-2L, "Super film mais j'ai oublié le pitch");
    }
}
