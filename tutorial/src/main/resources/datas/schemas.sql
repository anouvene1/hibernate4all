DROP TABLE IF EXISTS PUBLIC.MOVIE;
create table PUBLIC.MOVIE
(
    ID            INTEGER not null,
    NAME          CHARACTER VARYING,
    DESCRIPTION   CHARACTER LARGE OBJECT,
    CERTIFICATION INTEGER,
    constraint "MOVIE_pk"
        primary key (ID)
);

