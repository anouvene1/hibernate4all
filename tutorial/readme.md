
# Cycle de vie d'une entité
![](./documentation/cycle-de-vie-entite.png "Cycle de vie d'une entité")

## Exemple 
```java
Movie movie = new Movie();
movie.setName("Inception");
repository.persist(movie);
```

## Logger
DEBUG o.hibernate.SQL.logStatement drop table if exists Movie CASCADE
DEBUG o.hibernate.SQL.logStatement drop sequence if exists hibernate_sequence
DEBUG o.hibernate.SQL.logStatement create sequence hibernate_sequence start with 1 increment by 1
DEBUG o.hibernate.SQL.logStatement create table Movie (id bigint not null, name varchar(255), primary key (id))
TRACE c.h.t.r.MovieRepository.persist Before persist, entityManager.contains(): false
DEBUG o.hibernate.SQL.logStatement call next value for hibernate_sequence
TRACE c.h.t.r.MovieRepository.persist After persist, entityManager.contains(): true
DEBUG o.hibernate.SQL.logStatement insert into Movie (name, id) values (?, ?)
TRACE o.h.t.d.s.BasicBinder.bind binding parameter [1] as [VARCHAR] - [Inception]
TRACE o.h.t.d.s.BasicBinder.bind binding parameter [2] as [BIGINT] - [1]
DEBUG o.hibernate.SQL.logStatement drop table if exists Movie CASCADE
DEBUG o.hibernate.SQL.logStatement drop sequence if exists hibernate_sequence

## Dirty checking
C'est une fonctionnalité du contexte de persistance qui concerne les entités managed.

Pour illustrer cela, on va créer un service qui va nous permettre de modifier
un attribut du film.
Pour ce faire, on va créer un peu plus d'attributs.
Ex.: 
- une colonne description sur le film dans la base de données.
- une classe service MovieService sur la page juste de l'accès aux bases de données

On va dire à Spring que c'est un composant service.
et on va va créer une méthode de service public 'updateDescription(Long id, String description)', une méthode très simple pour l'instant qui update description.
afin d'illustrer le dirty checking
Déjà on va lui injecter tout simplement le repository privé 'MovieRepository'

### Class MovieService 
1. on va charger d'abord le film via la méthode find du service
2. on va lui changer sa description.
3. deuxième règle d'or: Gestion des transactions et des sessions.
On veut que se soit dans une seule session, donc on va ici démarrer la transaction au niveau du service,
on l'appelera dans un test unitaire du service.
   ![](./documentation/regle-d-or-session-transaction.png "Règle d'or session/transaction")
4. on va créer la classe de test du film 'FilmServiceTest' dans le paquage 'service'.
Dans un premier temps, on va injecter en premier lieu le service 'MovieService'.
Puis, on va récupérer la description du film, et on va l'updater
et pour finir 'Movie'.
   ![](./documentation/movieServiceTest.png "Test movie update description")

Et on va changer la description en super film.
on va faire un autre select pour récupérer le Movie.
Et une fois qu'on a ce select, on a cette entité qui est en état managed.
Ce qu'on va faire, c'est qu'on va changer sa description.
Pour l'instant, elle est nulle.
Comme on a changé un des attributs de Movie, Hibernate va se rendre compte
que cette entité est sale entre guillemets, c'est à dire qu'elle a subi un changement et va se charger
automatiquement d'envoyer au moment opportun la requête en base de données.

Ce moment opportun ça va être au moment du commit.
C'est à ce moment qu'hibernate va flusher sa session.
Et donc si on fait ça alors hop, on va continuer comme ça plutôt.

Vous voilà donc ce que vous pouvez voir ici.
C'est que la transaction est en train de se terminer.
Et on a flushé la session avec un update movie.

En fait, Hibernate va envoyer toutes les informations d'un coup à la base de données.
Il envoie tout l'objet en base de données.
Il ne va pas faire des envois ciblés.

Alors comment ça fonctionne, ce dirty checking?
Ce qu'il faut savoir, c'est que par défaut, quand on récupère une entité, Hibernate va le mettre dans sa map de session.
Parallèlement, il le met aussi dans une collection, une une sorte de photographie de l'état de cette entité.
Hibernate stocke en quelque sorte deux fois les entités que l'on charge.
Et ensuite, au moment du flush, il va faire une comparaison entre votre objet et sa photo prise et si il y a une différence, 
il va faire l'update en base de données.
![](./documentation/hibernate-flush-update.png "Flush/commoit et update")

### Si on résume donc le dirty checking.
On a vu que si on modifie une entité qui se trouve dans l'état managed, hibernate va repérer ces
modifications via le système de photos et va envoyer en base les modifications.

### Dirty checking avancé
Alors le dirty, c'est bien.
La première question qui revient souvent, c'est comment savoir si un objet est sale, c'est à dire
qu'il est vu comme modifié par Hibernate?

Alors ça, normalement on n'a pas à s'en soucier parce que c'est vraiment un mécanisme interne à Hibernate
et c'est à lui de gérer tout ça.

Maintenant, ce que l'on peut faire, c'est afficher des traces, par exemple pour voir
tout le mécanisme du dirty checking en action, on va aller dans logback test et on va rajouter ce
log sur "DefaultFlushEntityEventListener".
Il faut savoir qu'il y a beaucoup de mécanismes d'évènements dans Hibernate et ici on est sur un mécanisme
de "flush" par défaut.

Il est intéressant de voir dans Hibernate le mécanisme de SnapShot (photograhpie instantanée de l'état d'une entity).
Alors pour cela on va accéder à la session Hibernate.
Ce qui est important de voir, c'est quels sont les objets que Hibernate stocke ici.
Voilà donc ça c'est la fameuse map<class+ID, Entity> dont je vous parlais avec comme clé "class+ID" et l'entity la valeur qui correspond à tout
simplement la classe.
C'est dans cette map qu'Hibernate va stocker les snapshot, les photos des différentes entités.

Ce qu'on peut voir à travers ça, c'est tout simplement l'empreinte mémoire.
Quand vous confiez à la session Hibernate votre entité, l'empreinte mémoire est presque doublé, 
c'est-à-dire une entité en mémoire et une autre en version snapshot.
Attention tout de même à la performance si vous confiez trop d'objets dans la session
Hibernate. En termes de mémoire, ça peut faire mal.

Un autre point important avec Hibernate ce sont les mécanismes de dirty checking.
Auparavant, il existait un plugin fourni par Maven qui émule ce système de dirty checking d'Hibernate
C'est-à-dire, qu'à la compilation il va générer des proxies pour que lorsque vous fassiez des appels au serveur de vos entités. 
Il vérifie si la valeur est différente, alors il marque directement en live que l'entité est dirty.
Donc ça, ça va permettre à Hibernate d'éviter de faire la comparaison avec ses snapshot.
Il aura quand même en mémoire ces snapshots. Il ne va pas réduire l'empreinte mémoire, mais tout ce qui est CPU.
Par exemple si on a dix objets en mémoire, il va faire des comparaisons
champ à champs pour voir si l'entité a été modifiée ou non.

On n'aura donc plus besoin de faire ça avec ce plug in.
Malheureusement, ce plugin ne fonctionne plus avec la nouvelle version d'Hibernate.
Depuis pas mal de temps je crois(depuis 2018)

Ce qu'il faut donc retenir, c'est qu'il existe des techniques alternatives pour le dirty checking.
Ça va permettre de réduire le CPU, ainsi allez les utiliser si jamais vous avez des problèmes de performances
en termes de CPU.
Mais sinon normalement on n'en a pas besoin et le système par défaut convient très bien.

### Flush avancé
Nous avons vu que le Flush permettait à Hibernate, de synchroniser sa session avec
la base de données. C'est-à-dire que c'est au moment du Flush qu'il envoie toutes les différences vers la base de données.

On va étudier dans un premier temps quand Hibernate fait son Flush et dans un second
temps, le comment il le fait.

Le Quand c'est la stratégie du Flush appelé le Flush-Mode dans Hibernate.
(Voir l'exemple de test unitaire).
Dans ce test unitaire, on appelle une méthode créée pour l'occasion qui s'appelle
AddMovieSystem
GetAllDemonMovieService.

Cette méthode ouvre une transaction, donc on
va avoir aussi une session ouverte pour l'occasion.

Et dans cette méthode, on va créer un nouveau MovieFightClub qui vient s'ajouter aux
deux movie pré-existants pour les tests Memento et Inception.
Et ce movie, nous le persistons donc à ce moment-là.

Vous savez que la séquence Hibernate est appelée pour récupérer l'ID, mais Hibernate n'en voit pas encore l'insert
en base de données.
Ensuite, ce que nous avons dans cette méthode, c'est un return de repository getAll.

Donc nous voulons retourner en résultat l'ensemble des movies existants.
Souvenez-vous, dans le getAll, nous avons un "from movie", donc c'est une requête SQL classique.

On n'a pas encore vu dans le détail comment cele fonctionne.
Mais comme on a pu le voir, ça se traduit en "select * from ... " au niveau de la base de données.

A cet instant, Hibernate a un dilemme : il sait que dans sa session, il a un mouvement qui est en attente
d'être inséré en base de données et aussi il ne peut pas savoir s'il a l'ensemble des movie existants.
Donc, il est obligé de faire une requête "select * ..." en base de données pour pouvoir être sûr d'avoir
tous les movies.

Le problème, c'est que s'il fait un select à ce moment-là, il ne va pas récupérer le Movie FightClub
car il n'existe pas encore en base de données pour résoudre cette problématique et JPA instaure un
Flush-Mode auto. C'est la stratégie du Flush par défaut et cette stratégie force le Flush au moment du
commit bien entendu, mais aussi avant chaque query.

Et donc si on exécute notre test Flash mode, nous récupérons bien nos trois movies.
Le test est vert.
Et ici, si on examine en détails, c'est que, nous avons l'appel à la séquence et avant la requête "select
* from", nous avons l'insertion.

Alors ce mode auto ?
Généralement, on n'a pas à y toucher et ça fonctionne plutôt bien par défaut.

Maintenant, c'est toujours utile de savoir quand il envoie ses instructions en base de données.
Peut-être que dans certains de nos services, il vous faudra grouper les opérations d'insertion et de
modification d'une part et d'autre part, les opérations de sélection pour ne pas faire des créations.
Cependant, "sélection - création - sélection" va engendrer des successions de Flushs.

Ce qu'il faut savoir aussi, c'est que vous pouvez customiser Le Flush mode.
Mais attention, si vous y toucher, c'est en connaissance de cause.
Je vous ai fait un petit exemple ou ici dans ma configuration.

Je vais configurer le Flush-mode à la valeur commit ("org.hibernate.flushMode").
Maintenant le Flush ne se fera qu'au commit si on exécute notre test unitaire

D'après vous, qu'est ce qui va se passer ? ...
Notre test unitaire, bien sûr, va planter.

Pourquoi il va planter ?
Parce que au niveau de getAll, la session n'a pas encore été Flush et elle ne le sera qu'au commit.
Et donc nous n'allons pas récupérer le Movie Fight Club.

Comme vous pouvez le voir dans les traces, nous faisons ici un select avant de faire l'insertion.
Je profite de cet exemple pour faire un tout petit aparté pour ceux qui utiliseraient Hibernate sans JPA.

Il est possible en effet d'utiliser Hibernate sans utiliser le standard JPA.
Ça arrive notamment souvent dans des projets un peu anciens dans les entreprises.
En effet, le flush mode auto hibernate est comme le flush mode auto de JPA sauf qu'il ne fonctionnera pas avec
les requêtes SQL natives.

C'est à dire que dans un exemple similaire au nôtre, si le getAll (* FROM ...) ici et en fait une native query,
Et bien sûr que vous bootstrapper hibernate sans passer par JPA.

Alors ce test unitaire ne fonctionnera pas car il n'y aura pas de flush avant la requête native SQL.
Pour notre formation, je vous rassure, on utilise Hibernate en passant par JPA.

Donc le flush se fait avant toute requête et nous n'aurons pas ce genre de problème.

Maintenant que nous avons discuté du quand, on va discuter du comment il fait son flush pour parler du
comment on se retrouve avec la documentation officielle de Hibernate.
Cette documentation est plutôt bien foutue.
Ici, on est aux chapitres flush opération order.

Nous avons vu que le flush entraînait l'envoi de toutes les modifications latentes en base de données.
Maintenant, il peut être important de savoir dans quel ordre Hibernate envoie ces modifications.
Pour cela, dans la documentation, nous avons ici voilà un tout petit paragraphe qui a son importance
et qui parle de l'action qui représente la file d'attente des actions qui doit effectuer au moment
du flush.

Si vous avez des problèmes avec l'ordre d'exécution des requêtes SQL, c'est ici que vous devez venir.
On va illustrer ça tout de suite avec un nouveau test unitaire que j'ai aussi créé pour l'occasion.

Alors ce test unitaire appelle le MovieService.
Encore une fois et dans ce MovieService, nous avons une nouvelle méthode.

Alors cette méthode n'a pas vraiment de sens métier en soi, mais c'est vraiment pour illustrer un cas
qui pourrait vous arriver lorsque vous développez vos propres services métier.

Alors ce Remove AddMovie, qu'est ce qu'il fait?

Il supprime le Movie deux, alors le Movie -2 c'est celui là.
Ensuite il crée un movie avec le même nom et avec une description mise à jour au final
Puis il le persiste.

Alors si on exécute ce test unitaire.
Comme vous pouvez le voir, tout a l'air de bien fonctionner et a priori, dans un premier temps, il
n'y a pas l'air d'avoir de problème.

Mais pourtant, il y a un problème ici.
Je vais vous le montrer tout de suite.
On va rajouter au niveau de notre mapping le fait que la colonne "name" doit être unique, c'est à dire qu'il ne peut pas avoir deux films de même nom en base de données.

Si on execute notre test ici.
Et là, c'est le drame.

Le test est rouge et on obtient une exception on obtient une violation de contrainte unique SQL comme
vous pouvez le voir ici.

Unique index of primary cookie.
Violation.

Et la raison de cette violation provient directement de notre documentation ici.
On voit qu'il insert les entités d'abord, puis en tout dernier, il va faire le 18, c'est à dire que
ici nous avons notre appel à la séquence.

Nous avons l'insertion.
Ou il insère Memento et à ce niveau là, il n'a pas encore fait la suppression qu'on lui avait demandé
ici.

Et donc forcément, ça pète en base de données.
Alors bien sûr, si on voulait faire les choses correctement ici, on ne ferait pas un remove, puis
un persist du même movie.

On ferait plutôt une mise à jour, mais c'est pour illustrer le flush order et il pourrait vous arriver
une situation similaire dans un de vos cas métier qui serait un peu plus complexe.

Ce qu'il faut savoir, c'est que un petit peu comme avec le flush mode, généralement le comportement
d'Hibernate par défaut fonctionne plutôt bien et il est fait pour ça.

Mais il peut vous être utile de savoir quand même comment il fonctionne dans certains cas pour pouvoir
vous débrouiller si jamais un jour vous avez une erreur liée à ce fonctionnement.


### FLUENT ENTITY
La bonne pratique consiste à chaîner les méthodes (setters) et lesquelles retourneraient l'objet lui-même

