set referential_integrity false;
drop table if exists Review ;
drop table if exists Movie;
set referential_integrity true;

create table Movie
(
    name          VARCHAR not null,
    description   VARCHAR not null,
    certification INTEGER not null,
    id            INTEGER not null primary key
);
insert into Movie (name, description, certification, id) values ('Inception', '', 0, -1L); -- Numéro de séquence commençant à 0
insert into Movie (name, description, certification, id) values ('Un train pour busan', '', 1, -2L);
insert into Movie (name, description, certification, id) values ('Parasite', '', 2, -3L);
insert into Movie (name, description, certification, id) values ('La ligne verte', '', 3, -4L);


create table Review
(
    author   VARCHAR not null,
    content  VARCHAR not null,
    movie_id INTEGER not null,
    id       INTEGER not null primary key,
    foreign key(movie_id) references Movie
);
insert into Review (author, content, movie_id, id) values ( 'Maxime', 'Super film !', -1L, -1L );
insert into Review (author, content, movie_id, id) values ( 'Lorie', 'Bof bof !', -1L, -2L );

