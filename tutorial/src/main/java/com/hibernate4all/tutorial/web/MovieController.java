package com.hibernate4all.tutorial.web;

import com.hibernate4all.tutorial.domain.Movie;
import com.hibernate4all.tutorial.service.MovieJpaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("tutorial/api/movie")
public class MovieController {
    private final MovieJpaService movieJpaService;

    public MovieController(MovieJpaService movieJpaService) {
        this.movieJpaService = movieJpaService;
    }

    @PostMapping("/createMovie")
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        Movie savedMovie = this.movieJpaService.saveMovie(movie);
        return new ResponseEntity<>(savedMovie, HttpStatus.CREATED);
    }

    @GetMapping("/readMovie/{id}")
    public ResponseEntity<Movie> readMovie(@PathVariable Long id){
        //Optional<Movie> movie = this.movieJpaService.getMovie(id);
        Movie movie = this.movieJpaService.findMovie(id);
        if(movie != null)
            return new ResponseEntity<>(movie, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/readAllMovies")
    public ResponseEntity<List<Movie>> readAllMovies(){
        List<Movie> movies = this.movieJpaService.getAllMovies();

        if(movies.size() > 0)
            return new ResponseEntity<>(movies, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PutMapping("/updateMovie")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie movie) {
        Movie updatedMovie = this.movieJpaService.updateMovie(movie);
        if(updatedMovie != null) {
            return new ResponseEntity<>(updatedMovie, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }
    @PutMapping("/updateMovieDescription")
    public ResponseEntity<Integer> updateMovieDescription(@RequestBody Movie movie) {
        return new ResponseEntity<>(this.movieJpaService.updateMovieDescription(movie.getDescription(), movie.getCertification().ordinal(), movie.getId()), HttpStatus.OK);
    }

    @DeleteMapping("/deleteMovie/{movieId}")
    public void deleteMovie(@PathVariable Long movieId) {
        this.movieJpaService.deleteMovie(movieId);
    }
}
