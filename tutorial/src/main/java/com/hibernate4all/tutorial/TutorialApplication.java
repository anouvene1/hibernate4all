package com.hibernate4all.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
@ComponentScan(basePackages = {"com.hibernate4all.tutorial.*"})
public class TutorialApplication {
	public static void main(String[] args) {
		SpringApplication.run(TutorialApplication.class, args);
	}

}
