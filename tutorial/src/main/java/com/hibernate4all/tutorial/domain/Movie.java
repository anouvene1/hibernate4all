package com.hibernate4all.tutorial.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
//@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Movie {
   // public enum Certification {
   //     TOUS_PUBLIC, INTERDIT_MOINS_12, INTERDIT_MOINS_16, INTERDIT_MOINS_18
   // }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private String description;

    //@Enumerated // Cas 1 - pas top au niveau stockage d'un integer car l'ordre risque de changer si on ajoute d'autre element dans un enum
    //@Enumerated(EnumType.STRING) // Cas 2 - pas top au niveau stockage en bdd si chaine enum trop longue
    // Cas 3 - Utilisation d'un converter CertificationAttributeConverter avec l'interface AttributeConverter
    private Certification certification;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "movie")
    private List<Review> reviews = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public Movie setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Movie setDescription(String description) {
        this.description = description;
        return this;
    }

    public Certification getCertification() {
        return certification;
    }
    public Movie setCertification(Certification certification) {
        this.certification = certification;
        return this;
    }

    public List<Review> getReviews() {
        return Collections.unmodifiableList(reviews); // Empêcher la modification d'une liste de reviews
    }

    public Movie setReviews(List<Review> reviews) {
        this.reviews = reviews;
        return this;
    }

    public Movie addReview(Review review) {
        if(review != null) {
            reviews.add(review);
            review.setMovie(this);
        }
        return this;
    }

    public Movie renmoveReview(Review review) {
        if(review != null) {
            reviews.remove(review);
            review.setMovie(null);
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return getId().equals(movie.getId())
                && getName().equals(movie.getName())
                && getDescription().equals(movie.getDescription())
                && getCertification() == movie.getCertification()
                && getReviews().equals(movie.getReviews());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getCertification(), getReviews());
    }
}
