package com.hibernate4all.tutorial.repository;

import com.hibernate4all.tutorial.domain.Certification;
import com.hibernate4all.tutorial.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface MovieJpaRepository extends JpaRepository<Movie, Long> {
    @Modifying(flushAutomatically = true)
    @Query("update Movie m set m.description = :description, m.certification = :certificationKey where m.id = :id")
    Integer updateMovieDescription(
            @Param("description") String description,
            @Param("certificationKey") Certification certification,
            @Param("id") Long id);

}
