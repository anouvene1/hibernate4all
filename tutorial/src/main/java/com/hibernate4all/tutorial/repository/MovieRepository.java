package com.hibernate4all.tutorial.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hibernate4all.tutorial.domain.Movie;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class MovieRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieRepository.class);

    @PersistenceContext
    EntityManager entityManager;
    @Transactional
    public void persist(Movie movie) {
        LOGGER.trace("Before persist, entityManager.contains(): " + entityManager.contains(movie));
        entityManager.persist(movie);
        LOGGER.trace("After persist, entityManager.contains(): " + entityManager.contains(movie));
    }
    public Movie find(Long id) {
        Movie movie = entityManager.find(Movie.class, id);
        LOGGER.trace("After find, entityManager.contains(): " + entityManager.contains(movie));
        return movie;
    }
    public List<Movie> getAll() {
        return entityManager.createQuery("FROM Movie", Movie.class).getResultList();
    }
}
